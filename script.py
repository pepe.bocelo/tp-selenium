from dotenv import load_dotenv
import os
from selenium import webdriver
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.safari.options import Options as SafariOptions
from selenium.webdriver.edge.options import Options as EdgeOptions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from threading import Thread
import time
load_dotenv()
BROWSERSTACK_USERNAME = os.environ.get("BROWSERSTACK_USERNAME") or "pepebocelo_j0Pt5g"
BROWSERSTACK_ACCESS_KEY = os.environ.get("BROWSERSTACK_ACCESS_KEY") or "bxfMJXujiPTg19tBVaLC"
URL = os.environ.get("URL") or "https://hub.browserstack.com/wd/hub"
BUILD_NAME = "browserstack-build-1"
capabilities = [
    {
        "browserName": "chrome",
        "browserVersion": "103.0",
        "os": "Windows",
        "osVersion": "11",
        "sessionName": "BStack Python sample parallel", # test name
        "buildName": BUILD_NAME,  # Your tests will be organized within this build
    },
    {
        "browserName": "firefox",
        "browserVersion": "102.0",
        "os": "Windows",
        "osVersion": "10",
        "sessionName": "BStack Python sample parallel",
        "buildName": BUILD_NAME,
    },
    {
        "browserName": "safari",
        "browserVersion": "14.1",
        "os": "OS X",
        "osVersion": "Big Sur",
        "sessionName": "BStack Python sample parallel",
        "buildName": BUILD_NAME,
    },
]
def get_browser_option(browser):
    switcher = {
        "chrome": ChromeOptions(),
        "firefox": FirefoxOptions(),
        "edge": EdgeOptions(),
        "safari": SafariOptions(),
    }
    return switcher.get(browser, ChromeOptions())
def run_session(cap):
    bstack_options = {
        "osVersion" : cap["osVersion"],
        "buildName" : cap["buildName"],
        "sessionName" : cap["sessionName"],
        "userName": BROWSERSTACK_USERNAME,
        "accessKey": BROWSERSTACK_ACCESS_KEY
    }
    if "os" in cap:
      bstack_options["os"] = cap["os"]
    options = get_browser_option(cap["browserName"].lower())
    if "browserVersion" in cap:
      options.browser_version = cap["browserVersion"]
    options.set_capability('bstack:options', bstack_options)
    driver = webdriver.Remote(
        command_executor=URL,
        options=options)




#driver = webdriver.Firefox()



    driver.get("https://herukeado.herokuapp.com/index.html")

    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'nombre'))).send_keys("Felipe")

    driver.find_element(By.ID, "contraseña").send_keys("secret-password")

    driver.find_element(By.XPATH, '//a[@href="navbar.html"]').click()

    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'nombres'))).send_keys("Felipe")

    driver.find_element(By.ID, "apellidos").send_keys("Bocelo")

    driver.find_element(By.ID, "correo").send_keys("yoquese@gmail.com")

    driver.find_element(By.XPATH, '//a[@type="submit"]').click()

    try:
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, 'texto')))
        print("Test Succeed")

    except:
        print("Error: No cargo la pagina de Bienvenida")

    time.sleep(2)

    driver.quit()
for cap in capabilities:
  Thread(target=run_session, args=(cap,)).start()
